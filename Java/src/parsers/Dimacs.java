package parsers;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class Dimacs {

	//http://dimacs.rutgers.edu/
	//Format information: http://www.dis.uniroma1.it/challenge9/format.shtml
	
	public static void main(String[] args) throws Exception{
		transInterToDIMACS("/home/lg/Dropbox/Research/2016/RedundancyInSTP/example/50/ppc.intv");
//		System.out.println(Integer.parseInt("-1079.0"));
	}
	
	public static HashMap<Integer,Integer> readDIMACS(String fileName,int[] nV) throws Exception{
		HashMap<Integer,Integer> network = new HashMap<Integer,Integer>();
		int numVar = 0;
		BufferedReader br = new BufferedReader(new FileReader(fileName ));
		String line = "";
		if((line  = br.readLine()) != null){
			//assume the first line stores the numVar of the relations
			//#numVar:n
			numVar = Integer.parseInt(line.split("\\s")[2]);
			nV[0] = numVar;
		}
		while ((line  = br.readLine()) != null) {
			String[] pairAndRels = line.split("\\s");
			int i = Integer.parseInt(pairAndRels[1]);
			int j = Integer.parseInt(pairAndRels[2]);
			int r = Integer.parseInt(pairAndRels[3]);
			network.put(i*numVar+j, r);
		}
		if(br !=null){
			br.close();
		}
		return network;
	}
	
	public static HashMap<Integer,int[]> transBoundNet2IntervalNet() throws Exception{
		String fileName = "";
		int[] nV = new int[1];
		HashMap<Integer,Integer> boundNetwork = Dimacs.readDIMACS(fileName, nV);
		HashMap<Integer,int[]> intervalNetwork = new HashMap<Integer,int[]>();
		//translate boundNetwork to intervalNetwork
		int numVar = nV[0];
		Set<Entry<Integer,Integer>> entries = boundNetwork.entrySet();
		for(Entry<Integer,Integer> e : entries){
			int pairsSum = e.getKey();
			int pairi = pairsSum/numVar;
			int pairj = pairsSum % numVar;
			if(pairi < pairj){
				int upperbound = e.getValue();
				int lowerbound = 0 - boundNetwork.get(pairj*numVar+pairi);
				intervalNetwork.put(pairsSum, new int[]{lowerbound,upperbound});
			}
		}
		return intervalNetwork;
	}
	
	public static void transInterToDIMACS(String fileName) throws Exception{
		BufferedReader br = new BufferedReader(new FileReader(fileName ));
		String line = "";
		if((line  = br.readLine()) != null){
			//assume the first line stores the numVar of the relations
			//print first line
			System.out.println(line);
		}
		while ((line  = br.readLine()) != null) {
			//0 1 [3,4]
			String[] pairAndRels = line.split("\\s");
			int i = Integer.parseInt(pairAndRels[0]);
			int j = Integer.parseInt(pairAndRels[1]);
			String[] intervalS = pairAndRels[2].replaceAll("[\\[\\]]", "").split(",");
			if(!intervalS[1].contains("i")){
				int intu = (int) Double.parseDouble(intervalS[1]);
				System.out.println("a"+" "+i+" "+j+" "+intu);
			}
			if(!intervalS[0].contains("i")){
				int intl = (int) Double.parseDouble(intervalS[0]);
				System.out.println("a"+" "+j+" "+i+" "+(-intl));
			}
		}
		if(br !=null){
			br.close();
		}
	}
}
