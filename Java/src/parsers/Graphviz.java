package parsers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;

public class Graphviz {

	public static void main(String[] args) throws Exception {
		readDIMACSIntoDOT("/home/lg/Dropbox/Research/2016/RedundancyInSTP/Implementation/test/50Rev.dimacs");

//		readIntervalRepIntoDOT("/home/lg/Dropbox/Research/2016/RedundancyInSTP/Implementation/test/50RevPPC.intv");
		
//		getDatasetInfo("/home/lg/Downloads/js/");
//		reviseDIMACS("/home/lg/Dropbox/Research/2016/RedundancyInSTP/Implementation/example/50initial.dimacs",10);
	}
	
	public static void reviseDIMACS(String fileName,double div) throws Exception{
		BufferedReader br = new BufferedReader(new FileReader(fileName ));
		String line = "";
		if((line  = br.readLine()) != null){
			//print first line
			System.out.println(line);
		}
		while ((line  = br.readLine()) != null) {
			String[] pairAndRels = line.split("\\s");
			double r = Double.parseDouble(pairAndRels[3]);
			r = Math.round(r/div);
			System.out.println(pairAndRels[0]+" "+pairAndRels[1]+" "+pairAndRels[2]+" "+(int) r);
			
		}
		if(br !=null){
			br.close();
		}
	}

	public static void readDIMACSIntoDOT(String fileName) throws Exception{
		/*
		  a directed graph should be represented as 
			digraph {
			 { //nodes
			  0[label=<<I>x</I><SUB>1</SUB>>];
			  1[label=<<I>x</I><SUB>2</SUB>>];
			  2[label=<<I>x</I><SUB>3</SUB>>];
			 }
			 0 -> 1 [label="[-2,1]"];
			 0 -> 2 [label="[-2,1]"];
			}
		*/
		
		
		BufferedReader br = new BufferedReader(new FileReader(fileName ));
		String line = "";
		if((line  = br.readLine()) != null){
			//assume the first line stores the numVar of the relations
			int numV = Integer.parseInt(line.split("\\s")[2]);
			//print nodes
			System.out.println("digraph {");
			System.out.println("{");
			for(int i=0;i<numV;i++){
				System.out.println(i+"[label=<<I>x</I><SUB>"+(i+1)+"</SUB>>];");
			}
			System.out.println("}"); 
		}
		//print edges
		boolean first = true;
		int[] previousPair = {-1,-1};
		int[] currentPair = {0,0};
		String[] currentInterval = {"-inf","inf"};
//		String[] newInterval = {"-inf","inf"};
		while ((line  = br.readLine()) != null) {
			String[] pairAndRels = line.split("\\s");
			int i = Integer.parseInt(pairAndRels[1]);
			int j = Integer.parseInt(pairAndRels[2]);
			String r = pairAndRels[3];
			if(i < j){
				currentPair = new int[]{i,j};
				//if pair not changed
				if(Arrays.equals(previousPair, currentPair)){
					//update the current interval
					currentInterval[0] = r;
				}
				//if pair changed
				else{
					//if not the initial one
					if(!first){
						//print the current interval
						System.out.println(previousPair[0]+"->"+previousPair[1] +"["+"label=\""+Arrays.toString(currentInterval)+"\"];");
						
					}
					//re-initialize the current interval
					currentInterval = new String[]{"-inf",r};
					//change the previous pair to current pair
					previousPair = currentPair;
				}
			}
			else{
				currentPair = new int[]{j,i};
				//if pair not changed
				if(Arrays.equals(previousPair, currentPair)){
					//update the current interval
					if(r.startsWith("-")){
						currentInterval[0] = r.replace("-", "");
					}
					else{
						currentInterval[0] = "-"+r;
					}
					
				}
				//if pair changed
				else{
					//suppress the print of the initial one
					if(!first){
						//print the current interval
						System.out.println(previousPair[0]+"->"+previousPair[1] +"["+"label=\""+Arrays.toString(currentInterval)+"\"];");
					}
					//re-initialize the current interval
					currentInterval = new String[]{currentInterval[0],"inf"};
					//change the previous pair to current pair
					previousPair = currentPair;
				}
			}				
			first = false;
		}
		if(!first){
			System.out.println(previousPair[0]+"->"+previousPair[1] +"["+"label=\""+Arrays.toString(currentInterval)+"\"];");
		}
		if(br !=null){
			br.close();
		}
		System.out.println("}"); 
	}
	
	public static void readIntervalRepIntoDOT(String fileName) throws Exception{
		BufferedReader br = new BufferedReader(new FileReader(fileName ));
		String line = "";
		if((line  = br.readLine()) != null){
			//assume the first line stores the numVar of the relations
			int numV = Integer.parseInt(line.split("\\s")[2]);
			//print nodes
			System.out.println("digraph {");
			System.out.println("{");
			for(int i=0;i<numV;i++){
				System.out.println(i+"[label=<<I>x</I><SUB>"+(i+1)+"</SUB>>];");
			}
			System.out.println("}"); 
		}
		while ((line  = br.readLine()) != null) {
			String[] pairAndRels = line.split("\\s");
			String[] interval = pairAndRels[2].replaceAll("[\\[\\]]", "").split(",");
			double a = Double.parseDouble(interval[0]);
			double b = Double.parseDouble(interval[1]);
			System.out.println(pairAndRels[0]+"->"+pairAndRels[1] +"["+"label=\"["+((int) a)+","+((int) b)+"]\"];");
		}
		if(br !=null){
			br.close();
		}
		System.out.println("}"); 
	}
	
	public static void getDatasetInfo(String path) throws Exception{
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();
		int numF = listOfFiles.length;
		int[] numVArray = new int[numF];
		int[] numEArray = new int[numF];

	    for (int i = 0; i < numF; i++) {
	      if (listOfFiles[i].isFile()) {
	    	  //read the content
	    	  BufferedReader br = new BufferedReader(new FileReader(listOfFiles[i]));
	    	  String line = "";
	    	  while((line  = br.readLine()) != null){
	    		  if(line.startsWith("p")){
	    			  String[] temp = line.split("\\s");
	    			  numVArray[i] = Integer.parseInt(temp[2]);
	    			  numEArray[i] = Integer.parseInt(temp[3]);
	    			  break;
	    		  }
	    	  }
	    	  if(br !=null){
	  			br.close();
	    	  }
	    	  System.out.println(i+","+listOfFiles[i].getName()+","+numVArray[i]+","+numEArray[i]);
//	        System.out.println("File " + listOfFiles[i].getName());
	      } 
	    }
	}
	
}
